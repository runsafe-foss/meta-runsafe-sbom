# meta-runsafe-sbom

This layer generates a CycloneDX v1.4 compatible Software Bill of Materials for a yocto build.

## Installation

NOTE: This layer currently requires RunSafe Gremlin to be setup for the repository generating the SBOM. If you have not setup Gremlin or do not plan to use Gremlin, please contact support@runsafesecurity.com for instructions.

1. Download the latest `meta-runsafe-sbom` layer to the proper directory in your Yocto project, such as the top level of the poky directory.

> $ git clone -b master https://gitlab.com/runsafe-foss/meta-runsafe-sbom.git

2. Add the `meta-runsafe-sbom` layer to your `bblayers.conf` file.

> $ bitbake-layers add-layer meta-runsafe-sbom

## SBOM File Location

The SBOM is generated in `build/tmp/deploy/runsafe-sbom/target_sbom.cdx.json`.

## Configuration

The metadata component at the top of the CycloneDX SBOM can be configured by setting the following environment variables:

RUNSAFE_SBOM_METADATA_COMPONENT_NAME     : Name of the software or firmware being built. Defaults to yocto-image
RUNSAFE_SBOM_METADATA_COMPONENT_VERSION  : Version of the software or firmware being built. Defaults to 1.0.0
RUNSAFE_SBOM_METADATA_COMPONENT_SUPPLIER : Name of the supplier. Defaults to Organization: OpenEmbedded ()


## License

This layer is licensed under the MIT license. See LICENSE for more information. The layer was forked and modified from the bgnetworks/meta-dependency-track repo.
# SPDX-License-Identifier: MIT
# Copyright 2024 RunSafe Security, Inc.
# Copyright 2022 BG Networks, Inc.

# The product name that the CVE database uses.  Defaults to BPN, but may need to
# be overriden per recipe (for example tiff.bb sets CVE_PRODUCT=libtiff).
CVE_PRODUCT ??= "${BPN}"
CVE_VERSION ??= "${PV}"

RUNSAFE_SBOM_DIR ??= "${DEPLOY_DIR}/runsafe-sbom"
RUNSAFE_SBOM_FILE ??= "${RUNSAFE_SBOM_DIR}/target_sbom.cdx.json"
RUNSAFE_TMP ??= "${TMPDIR}/runsafe-sbom"
RUNSAFE_LOCK ??= "${RUNSAFE_TMP}/sbom.lock"

SPDX_ORG ??= "OpenEmbedded ()"
SPDX_SUPPLIER ??= "Organization: ${SPDX_ORG}"

python do_runsafe_sbom_init() {
    import uuid
    from datetime import datetime

    sbom_dir = d.getVar("RUNSAFE_SBOM_DIR", True)
    bb.debug(2, "Creating cyclonedx directory: " + sbom_dir)
    bb.utils.mkdirhier(sbom_dir)

    bb.debug(2, "Initializing sbom")

    oenv = d.getVar("BB_ORIGENV", False)
    component_name = oenv.getVar("RUNSAFE_SBOM_METADATA_COMPONENT_NAME", True) or "yocto-image"
    supplier = oenv.getVar("RUNSAFE_SBOM_METADATA_COMPONENT_SUPPLIER", True) or d.getVar("SPDX_SUPPLIER", True)
    version = oenv.getVar("RUNSAFE_SBOM_METADATA_COMPONENT_VERSION", True) or "1.0.0"

    yocto_version = d.getVar("DISTRO_VERSION", True)
    yocto_cpe = "cpe:2.3:a:linuxfoundation:yocto:" + str(yocto_version) + ":*:*:*:*:*:*:*"

    offline_mode = oenv.getVar("RUNSAFE_OFFLINE_ONLY", False)

    timestamp = datetime.now().utcnow().isoformat()
    if timestamp[-1] != "Z":
        timestamp += "Z"

    diag_info = """ Configuring RunSafe SBOM environment

RunSafe Diagnostic Report:
RUNSAFE_SBOM_DIR                         = {0}
RUNSAFE_OFFLINE_ONLY                     = {1}
RUNSAFE_SBOM_METADATA_COMPONENT_NAME     = {2}
RUNSAFE_SBOM_METADATA_COMPONENT_VERSION  = {3}
RUNSAFE_SBOM_METADATA_COMPONENT_SUPPLIER = {4}
    """.format(sbom_dir, offline_mode, component_name, version, supplier)
    bb.note(diag_info)

    write_sbom(d, {
        "bomFormat": "CycloneDX",
        "specVersion": "1.4",
        "serialNumber": "urn:uuid:" + str(uuid.uuid4()),
        "version": 1,
        "metadata": {
            "component": {
                "bom-ref": str(uuid.uuid4()),
                "name": component_name,
                "supplier": {
                    "name": supplier
                },
                "type": "firmware",
                "version": version
            },
            "timestamp": timestamp,
            "tools": [
                {
                    "name": "meta-runsafe-sbom",
                    "vendor": "RunSafe Security, Inc.",
                    "version": "1.0.0",
                    "externalReferences": [
                        {
                            "url": "https://app.runsafesecurity.com",
                            "type": "website"
                        },
                        {
                            "url": "mailto:support@runsafesecurity.com",
                            "type": "support"
                        }
                    ]
                }
            ]
        },
        "components": [
            {
                "bom-ref": str(uuid.uuid4()),
                "cpe": yocto_cpe,
                "licenses": [
                    {
                        "expression": "MIT & GPL-2.0-only"
                    }
                ],
                "name": "yocto",
                "properties": [],
                "supplier": {"name": "Organization: OpenEmbedded ()"},
                "type": "operating-system",
                "version": yocto_version
            }
        ]
    })
}
addhandler do_runsafe_sbom_init
do_runsafe_sbom_init[eventmask] = "bb.event.BuildStarted"

python do_runsafe_sbom_collect_component() {
    # This layer currently only produces SBOMs for software going onto the target, so we skip native recipes
    if bb.data.inherits_class("native", d):
        return

    import uuid
    try:
        import oe.cve_check.get_cpe_ids as get_cpe_ids
    except:
        # The implementation of this function is from the poky repo
        # commit 28fd497a26bdcc12d952f81436a6d873d81cd462
        # and is licensed under the MIT license
        def get_cpe_ids(cve_product, version):
            """
            Get list of CPE identifiers for the given product and version
            """

            version = version.split("+git")[0]

            cpe_ids = []
            for product in cve_product.split():
                # CVE_PRODUCT in recipes may include vendor information for CPE identifiers. If not,
                # use wildcard for vendor.
                if ":" in product:
                    vendor, product = product.split(":", 1)
                else:
                    vendor = "*"

                cpe_id = 'cpe:2.3:*:{}:{}:{}:*:*:*:*:*:*:*'.format(vendor, product, version)
                cpe_ids.append(cpe_id)

            return cpe_ids

    provides = d.getVar("PROVIDES", True) + " " + d.getVar("PN", True) + " " + d.getVar("BPN", True)
    supplier = d.getVar("SPDX_SUPPLIER", True)
    version = d.getVar("PV", True)

    lic = d.getVar("LICENSE", True)
    licenses = []
    if "PD" in lic:
        licenses.append({"license": {"name": lic}})
    else:
        licenses.append({"expression": lic})

    bdepends = d.getVar("DEPENDS", True)
    if bdepends:
        bdepends = " ".join([x for x in bdepends.split() if not ("-native" in x or "-cross" in x)])
    else:
        bdepends = ""

    rdepends = d.getVar("RDEPENDS", True)
    if rdepends:
        rdepends = " ".join([x for x in rdepends.split() if not ("-native" in x or "-cross" in x)])
    else:
        rdepends = ""

    sbom = read_sbom(d)

    # Create component with package metadata
    cve_product = d.getVar("CVE_PRODUCT", True)
    names = cve_product.split()
    typ = "library"
    for index, cpe in enumerate(get_cpe_ids(cve_product, d.getVar("CVE_VERSION", True))):
        bb.debug(2, "Collecting package " + cve_product + "@" + str(version) + " (" + cpe + ")")
        match = next((component for component in sbom["components"] if component["cpe"] == cpe), None)
        if match:
            component_provides = next((prop for prop in match["properties"] if prop["name"] == "runsafe-sbom-provides"), None)
            if component_provides:
                new_provides = component_provides["value"] + " " + provides
                component_provides["value"] = " ".join(set(new_provides.split()))
        else:
            sbom["components"].append({
                "bom-ref": str(uuid.uuid4()),
                "cpe": cpe,
                "licenses": licenses,
                "name": names[index],
                "properties": [
                    {
                        "name": "runsafe-sbom-depends",
                        "value": rdepends + " " + bdepends
                    },
                    {
                        "name": "runsafe-sbom-provides",
                        "value": provides
                    }
                ],
                "supplier": {"name": supplier},
                "type": typ,
                "version": version
            })

    write_sbom(d, sbom)
}
addtask do_runsafe_sbom_collect_component after do_image before do_image_complete
do_runsafe_sbom_collect_component[nostamp] = "1"
do_runsafe_sbom_collect_component[lockfiles] += "${RUNSAFE_LOCK}"
do_image_complete[recrdeptask] += "do_runsafe_sbom_collect_component"

python do_runsafe_sbom_upload () {
    import subprocess
    import os

    sbom = read_sbom(d)
    sbom["dependencies"] = []

    top_level_refs = {}
    top_level_refs["ref"] = sbom["metadata"]["component"]["bom-ref"]
    top_level_refs["dependsOn"] = []

    comp_refs = {}
    for component in sbom["components"]:
        comp_refs[component["name"]] = component["bom-ref"]

        for prop in component["properties"]:
            if prop["name"] == "runsafe-sbom-provides":
                for provides in prop["value"].split():
                    comp_refs[provides] = component["bom-ref"]

        top_level_refs["dependsOn"].append(component["bom-ref"])

    sbom["dependencies"].append(top_level_refs)

    for component in sbom["components"]:
        dep = {}
        dep["ref"] = component["bom-ref"]
        dep["dependsOn"] = set()

        for prop in component["properties"]:
            if prop["name"] == "runsafe-sbom-depends":
                for depends in prop["value"].split():
                    try:
                        if dep["ref"] == comp_refs[depends]:
                            continue
                        dep["dependsOn"].add(comp_refs[depends])
                    except:
                        bb.warn("Unable to find dependency " + depends + " for component " + component["name"])
        del component["properties"]

        dep["dependsOn"] = list(dep["dependsOn"])
        sbom["dependencies"].append(dep)

    write_sbom(d, sbom)

    # Upload to RunSafe Portal
    env = os.environ
    oenv = d.getVar("BB_ORIGENV", False)

    runsafe_offline_only = oenv.getVar("RUNSAFE_OFFLINE_ONLY", False)
    if runsafe_offline_only == "1":
        return

    runsafe_sbom_server = oenv.getVar("RUNSAFE_SBOM_SERVER", False)
    runsafe_license_key = oenv.getVar("RUNSAFE_LICENSE_KEY", False)
    runsafe_audit_id = oenv.getVar("RUNSAFE_AUDIT_ID", False)
    runsafe_sbom_id = oenv.getVar("RUNSAFE_SBOM_ID", False)

    if runsafe_sbom_server is None or runsafe_license_key is None or runsafe_audit_id is None:

        error_msg = """

Unable to initiate SBOM upload to RunSafe Portal. Please ensure the environment
variables RUNSAFE_SBOM_SERVER, RUNSAFE_LICENSE_KEY, and RUNSAFE_AUDIT_ID are set.
Contact support@runsafesecurity.com for additional support.

If this was unintentional, please verify RUNSAFE_OFFLINE_ONLY=1 according to the
diagnostic report displayed at the start of this build. No data was sent prior to
this error message.
"""
        bb.fatal(error_msg)

    env["RUNSAFE_SBOM_SERVER"] = runsafe_sbom_server
    env["RUNSAFE_LICENSE_KEY"] = runsafe_license_key
    env["RUNSAFE_AUDIT_ID"] = runsafe_audit_id

    if runsafe_sbom_id is None:
        runsafe_sbom_id = subprocess.check_output(["runsafe_sbom_cli","start-sbom"], env=env).decode()

    env["RUNSAFE_SBOM_ID"] = runsafe_sbom_id

    subprocess.run(["runsafe_sbom_cli","upload-sbom","-i", d.getVar("RUNSAFE_SBOM_FILE", True)], env=env, check=True)
}
addhandler do_runsafe_sbom_upload
do_runsafe_sbom_upload[eventmask] = "bb.event.BuildCompleted"

def read_sbom(d):
    import json
    fp = open(d.getVar("RUNSAFE_SBOM_FILE", True), "r")
    return json.load(fp)

def write_sbom(d, sbom):
    import json
    with open(d.getVar("RUNSAFE_SBOM_FILE", True), "w") as sbom_file:
        sbom_file.write(json.dumps(sbom, indent=2))
